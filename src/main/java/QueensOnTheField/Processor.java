package QueensOnTheField;

import java.lang.*;

public class Processor {
    private int n, ncur;
    private int[][] field;
    private static final int FREE = 9;
    private static final int QUEEN = 1;
    private static final int RESTRICTED = 0;

    Processor(int a){
        n = a;
        field = new int[n][n];
        ncur = n;
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                field[i][j]=FREE;
            }
        }
        positionSearch(field, 0);
    }

    private void positionSearch(int[][] fldcur, int k){
        for (int i = k; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if( fldcur[i][j] == FREE ) {

                    int[][] fld = fldcur.clone();
                    for(int l=0; l< fld.length; l++){
                        fld[l] = fldcur[l].clone();
                    }

                    fld[i][j] = QUEEN;
                    ncur--;
                    positionSearch(restrictions(fld, i, j), i);
                }
            }
        }
        if(ncur==0){
            print(fldcur);
            System.exit(0);
        }else{
            ncur++;
        }
    }

    private int[][] restrictions(int[][] fldcur, int k, int m){
        for(int i=0; i<n; i++){
            if(i!=k){
                fldcur[i][m]=RESTRICTED;
            }
            if(i!=m){
                fldcur[k][i]=RESTRICTED;
            }
        }
        for(int i=1; i<n; i++){
            if( ((k-i)>-1)&&((m-i)>-1) ){
                fldcur[k-i][m-i]=RESTRICTED;
            }
            if( ((k-i)>-1)&&((m+i)<n) ) {
                fldcur[k-i][m+i]=RESTRICTED;
            }
            if( ((k+i)<n)&&((m-i)>-1) ){
                fldcur[k+i][m-i]=RESTRICTED;
            }
            if( ((k+i)<n)&&((m+i)<n) ){
                fldcur[k+i][m+i]=RESTRICTED;
            }
        }
        return (fldcur);
    }

    private void print(int[][] fld){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(fld[i][j]+"\t");
            }
            System.out.println();
        }
    }

}
