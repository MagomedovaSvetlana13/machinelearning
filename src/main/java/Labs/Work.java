package Labs;

import java.lang.*;
import java.util.*;

public class Work {
    private ArrayList<Node> vars = new ArrayList<>();
    private ArrayList<Node> minvars = new ArrayList<>();
    private ArrayList<Node> all = new ArrayList<>();
    private Node ways[][] = new Node[13][14];
    private int km[] = {0, 1, 1, 0, 1, 1, 2, 0, 0, 2};
    private boolean addEl;

    private boolean fillAndCheck(int c1, int m1, int c2, int m2, boolean sd){
        //проверка на совместимость состояния и элемента km (не вычитать из 0 и пр)
        if( (c1>=0)&(c2>=0)&(m1>=0)&(m2>=0)&(c1<=3)&(c2<=3)&(m1<=3)&(m2<=3)&((c1<=m1)||(m1==0))&((c2<=m2)||(m2==0)) ){
            Node Obj = new Node();
            Obj.cnb1 = c1;
            Obj.msn1 = m1;
            Obj.cnb2 = c2;
            Obj.msn2 = m2;
            Obj.side = sd;
            vars.add(Obj);
            addEl = true;
            return(true);
        } else{
            return (false);
        }
    }

    public void searchMainBranch(){
        fillAndCheck(3,3,0,0, false);
        fillAndCheck(2,2,1,1, false);
        fillAndCheck(2,2,1,1, true);
        fillAndCheck(1,1,2,2, false);
        fillAndCheck(1,1,2,2, true);
        fillAndCheck(3,0,0,3, false);
        fillAndCheck(0,3,3,0, true);
        fillAndCheck(2,3,1,0, false);
        fillAndCheck(1,3,2,0, false);
        fillAndCheck(1,3,2,0, true);
        fillAndCheck(2,0,1,3, false);
        fillAndCheck(2,0,1,3, true);
        fillAndCheck(1,0,2,3, true);
        all.addAll(vars);       //создание листа со всеми возможными состояниями
        vars.clear();

        fillAndCheck(3, 3, 0, 0, false);
        search();
    }

    private void search() {
        while( !((minvars.size()!=0) & (vars.size()==0))){
            if(vars.size()!=0) {
                for (int k = 0; k < 10; k += 2) {
                    addEl = false;
                    boolean curSd = false;
                    if (vars.size() == 1) {
                        curSd = vars.get(0).side;
                    }
                    if (vars.size() > 1) {
                        curSd = vars.get(vars.size() - 1).side;
                    }
                    if (!curSd) {   //если лодка с левой стороны
                        //избегаем недопустимые состояния
                        if ((!(((vars.get(vars.size() - 1).cnb1 - km[k]) == 2) & ((vars.get(vars.size() - 1).msn1 - km[k + 1]) == 3))) &
                                (!(((vars.get(vars.size() - 1).cnb1 - km[k]) == 3) & ((vars.get(vars.size() - 1).msn1 - km[k + 1]) == 0)))) {
                            if ( fillAndCheck(vars.get(vars.size() - 1).cnb1 - km[k], vars.get(vars.size() - 1).msn1 - km[k + 1],
                                    vars.get(vars.size() - 1).cnb2 + km[k], vars.get(vars.size() - 1).msn2 + km[k + 1], true) ) {
                                vars.get(vars.size() - 1).it = k;
                            }
                        }
                    } else {       //если лодка с правой стороны
                        //избегаем недопустимые состояния
                        if ((!(((vars.get(vars.size() - 1).cnb1 + km[k]) == 0) & ((vars.get(vars.size() - 1).msn1 + km[k]) == 3))) &
                                (!(((vars.get(vars.size() - 1).cnb1 + km[k]) == 1) & ((vars.get(vars.size() - 1).msn1 + km[k]) == 0)))) {
                            if (fillAndCheck(vars.get(vars.size() - 1).cnb1 + km[k], vars.get(vars.size() - 1).msn1 + km[k + 1],
                                    vars.get(vars.size() - 1).cnb2 - km[k], vars.get(vars.size() - 1).msn2 - km[k + 1], false) ) {
                                vars.get(vars.size() - 1).it = k;
                            }
                        }
                    }
                    if (addEl) {
                        if ((vars.get(vars.size() - 1).cnb1 == 0) & (vars.get(vars.size() - 1).msn1 == 0)) { //поиск min пути
                            if ((minvars.size() > vars.size()) || (minvars.size() == 0)) {   //заполняем minvars
                                minvars.clear();
                                minvars.addAll(vars);
                            }
                            while (vars.size() != (minvars.size() - 2)) {   //подчищаем vars
                                k = vars.get(vars.size() - 1).it;
                                vars.remove(vars.size() - 1);
                            }
                        } else {
                            if ((vars.size() >= minvars.size()) & (minvars.size() > 0)) {//если текущий путь уже >= min
                                while (vars.size() != (minvars.size() - 2)) {
                                    k = vars.get(vars.size() - 1).it;
                                    vars.remove(vars.size() - 1);
                                }
                            } else {
                                if (compare()) {
                                    k = -2;
                                }
                            }
                        }
                    }
                    while (k == 8) {
                        k = vars.get(vars.size() - 1).it;
                        vars.remove(vars.size() - 1);
                    }
                }
            }
        }
        fillWays();
    }

    private void fillWays(){
        if( ways[12][0]==null) {
            boolean notExist = true;
            for (int i = 0; i < (minvars.size() - 1); i++) {
                for (int j = 0; j < 13; j++) {
                    if(ways[j][0]!=null) {
                        if ((ways[j][0].cnb1 == minvars.get(i).cnb1) & (ways[j][0].msn1 == minvars.get(i).msn1) &
                                (ways[j][0].side == minvars.get(i).side)) {
                            notExist = false;
                        }
                    }
                }
                int l = 0;
                if (notExist) {
                    for(int j=0; j<13; j++){
                        if (ways[j][0] == null) {
                            l=j; break;
                        }
                    }
                    int j = 0;
                    for (int k = i; k < minvars.size(); k++) {      //заполнение массива с путями
                        ways[l][j] = minvars.get(k);
                        j++;
                    }
                }
            }
            minvars.clear();
            vars.clear();
            for (Node i : all) {
                notExist = true;
                for (int j = 0; j < 13; j++) {
                    if(ways[j][0]!=null) {
                        if ((i.cnb1 == ways[j][0].cnb1) & (i.msn1 == ways[j][0].msn1) & (i.side == ways[j][0].side)) {
                            notExist = false;
                        }
                    }
                }
                if (notExist) {
                    fillAndCheck(i.cnb1, i.msn1, i.cnb2, i.msn2, i.side);
                    search();
                }
            }
        }
    }

    public void selectWayLength(int c1, int m1, int c2, int m2, boolean sd){
        vars.clear();
        if( fillAndCheck(c1, m1, c2, m2, sd) ){
            int j=0;
            for(int i=0; i<13; i++){
                if( (ways[i][0].cnb1==c1)&(ways[i][0].msn1==m1)&(ways[i][0].side==sd) ){
                    while( ways[i][j]!=null ){
                        System.out.println(ways[i][j].cnb1+" "+ways[i][j].msn1+" "+ways[i][j].cnb2+" "+ways[i][j].msn2+" "+ways[i][j].side);
                        j++;
                    }
                }
            }
            System.out.println( "Length = " + (j-1) );
        }else{
            System.out.println("E R R O R : недопустимые значения!");
        }

    }

    private boolean compare(){
        if(vars.size()==1){
            return true;
        }
        for(int i=0; i<vars.size()-1; i++ ){ //проверка на совпадение состояний
            if( (vars.get(vars.size()-1).cnb1==vars.get(i).cnb1)&(vars.get(vars.size()-1).msn1==vars.get(i).msn1)&
                    (vars.get(vars.size()-1).cnb2==vars.get(i).cnb2)&(vars.get(vars.size()-1).msn2==vars.get(i).msn2)
                    &( vars.get(vars.size()-1).side==vars.get(i).side ) ){
                vars.remove(vars.size()-1);
                return false;
            }
        }
        return true;
    }

    private class Node {
        private int cnb1 = 0;
        private int msn1 = 0;
        private int cnb2 = 0;
        private int msn2 = 0;
        private int it =10;
        private boolean side = false; //лодка на левой стороне; true - на правой
    }
}