package Labs;

import java.lang.*;
import java.lang.Math.*;
import java.util.*;

public class App 
{
    public static void main( String[] args )
    {
        Work Obj = new Work();
        Obj.searchMainBranch();

        Scanner in = new Scanner(System.in);
        int c1, m1, c2, m2;
        boolean sd;
        System.out.println("Введите значения начального состояния в следующей последовательности:");
        System.out.println(" - каннибалы на левом берегу (от 0 до 3 вкл-но)");
        System.out.println(" - миссионеры на левом берегу (от 0 до 3 вкл-но)");
        System.out.println(" - каннибалы на правом берегу (от 0 до 3 вкл-но)");
        System.out.println(" - миссионеры на правом берегу (от 0 до 3 вкл-но)");
        System.out.println(" - сторона, на которой находится лодка в данный момент (false - левый берег, true - правый)");
        c1 = in.nextInt();
        m1 = in.nextInt();
        c2 = in.nextInt();
        m2 = in.nextInt();
        sd = in.nextBoolean();
        Obj.selectWayLength(c1, m1, c2, m2, sd);
    }
}
